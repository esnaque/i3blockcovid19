# i3blockCovid19

Block for i3blocks showing data of current cases of Coronavirus in your country
Data extracted from https://corona-stats.online/

Add the content of i3blocks.conf to your i3blocks.conf 
The current country is Spain. To change country see the list of countries in https://corona-stats.online/
and replace the countryi(Spain) in the command section of the block for your country.

T is total cases
R recovered
D deaths
A active cases
